defmodule TestCam.Router do
  use Plug.Router

  require Logger

  plug(:match)
  plug(:dispatch)

  @boundary "w58EW1cEpjzydSCq"

  get "/" do
    markup = """
    <html>
    <head>
      <title>Picam Video Stream</title>
      <script>
        function displayTime() {
          var date = new Date()
          var convertedDate = date.toUTCString();
          document.getElementById("time").innerHTML = convertedDate;
          setTimeout(displayTime, 500);
        }
      </script>
      <style>
        img {
          display: block;
        }
        span {
          display: block;
          font-size:25px;
        }
      </style>
    </head>
    <body onload="displayTime()">
      <img src="/video.mjpg" />
      <span id="time"><>
    </body>
    </html>
    """

    conn
    |> put_resp_header("Content-Type", "text/html")
    |> send_resp(200, markup)
  end

  forward("/video.mjpg", to: TestCam.Streamer)

  match _ do
    send_resp(conn, 404, "Oops. Try /")
  end
end
